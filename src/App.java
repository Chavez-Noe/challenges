public class App {
    public static void main(String[] args) throws Exception {
    
        //System.out.println(aplicarFormato("144641895"));
        //Double[] valores = {8D, 4D, 9D, 10D};
        //System.out.println(obtenerPromedio(valores));
        obtenerSumaNumParesImpares("8322946");
    }

    public static String aplicarFormato(String valor) {
        String cifra = valor;
        int index = valor.length() - 1;
        System.out.println("INDEX " + index);
        char i = valor.charAt(index);
        System.out.println("I " + i);
        String cifraFormato = "";
        int contadorCifra = 1;
        int codigoASCII = i;

        while (codigoASCII >= 48 && i <= 57) {
            System.out.println("***** ENTRANDO AL WHILE ******");
            cifraFormato = "" + i + cifraFormato; 
            System.out.println("CIFRA FORMATO " + cifraFormato);
            if (contadorCifra % 3 == 0 && i != cifra.charAt(0)) {
                System.out.println("***** ENTRANDO AL IF *****");
                cifraFormato = "," + cifraFormato;
                System.out.println("CIFRA FORMATO " + cifraFormato);
            }    
            contadorCifra++;
            System.out.println("CONTADOR CIFRA " + contadorCifra);
            index--;
            System.out.println("INDEX " + index);
            if (contadorCifra > valor.length()) {
                System.out.println("Ya no se apunta a más lementos dentro de la cadena");
                break;
            }
            i = valor.charAt(index);
            codigoASCII = i;
            System.out.println("I (CON EL SIGUIENTE VALOR A LA IZQUIERDA) " + i);
        }

        return cifraFormato;
    }

    public static Double obtenerPromedio(Double[] num) {
        Double total = 0D;
        int i = 1;
        Double valor = 0D;
        Double promedio = 0D;
        while (i <= num.length) {
            System.out.println("***********ENTRAMOS AL WHILE***************");
            valor = num[i - 1];
            System.out.println("VALOR --> " + valor);
            total += valor;
            System.out.println("TOTAL --> " + total);
            i++;
        }
        promedio = (double) (total / num.length);
        System.out.println("PROMEDIO --> " + promedio);
        return promedio;
    }

    public static void obtenerSumaNumParesImpares(String numero) {
        int contador = 0;
        char i = numero.charAt(contador);
        int iNum = Character.getNumericValue(i);
        int pares = 0;
        int impares = 0;
        int codigoASCII = i;
        while (codigoASCII >= 48 && i <= 57) {
            System.out.println("****** CICLO WHILE ********");
            System.out.println("I -->" + numero.charAt(contador)); 
            System.out.println("Inum --> " + iNum);
            if (iNum % 2 == 0) {
                pares += iNum;
                System.out.println("PARES --> " + pares);
            } else {
                impares += iNum;
                System.out.println("IMPARES --> " + impares);
            }
            contador++;
            if (contador == numero.length()) {
                break;
            }
            i = numero.charAt(contador);
            iNum = Character.getNumericValue(i);
            System.out.println("CONTADOR --> " + contador);
            codigoASCII = i;
            System.out.println("CODIGOANCII -->" + codigoASCII);
        }
        System.out.println("Pares: " + pares);
        System.out.println("Impares: " + impares);
    }

}
